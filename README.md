# AlgoComparer

## Introduction
This project visualizes 3 sorting algorithms and comes with 4 different data sets.

The different included algorithms:
- Quick Sort
- Bubble Sort
- Cycle Sort

## Preview
![image](/uploads/b5a020e6991367b5635fb3e307a13d7b/image.png)

## Installation
```
npm install
```

## Run project
```
npm run dev
```

## Build project
```
npm run build
```