onmessage = (event) => {
  const unsortedArray: number[] = JSON.parse(event.data.array)

  const quickSort = (arr: number[], low: number, high: number): number[] => {
    if (low < high) {
      const partitionIndex = partition(arr, low, high)
      quickSort(arr, low, partitionIndex - 1)
      quickSort(arr, partitionIndex + 1, high)
    }
    return arr
  }

  const partition = (arr: number[], low: number, high: number): number => {
    const pivot = arr[high]
    let i = low - 1

    for (let j = low; j < high; j++) {
      if (arr[j] < pivot) {
        i++
        ;[arr[i], arr[j]] = [arr[j], arr[i]]
        // Send an update after the swap
        const partialArray = [...arr]
        postMessage({ array: partialArray })
      }
    }

    ;[arr[i + 1], arr[high]] = [arr[high], arr[i + 1]]
    const partialArray = [...arr]
    postMessage({ array: partialArray })

    return i + 1
  }
  const sortedArray = quickSort(unsortedArray, 0, unsortedArray.length - 1)

  // Send the fully sorted array after all updates
  postMessage({ array: sortedArray })
}
