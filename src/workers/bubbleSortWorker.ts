onmessage = (event) => {
  const unsortedArray: number[] = JSON.parse(event.data.array)

  const bubbleSort = (arr: number[]): number[] => {
    const n = arr.length
    let swapped
    do {
      swapped = false
      for (let i = 0; i < n - 1; i++) {
        if (arr[i] > arr[i + 1]) {
          ;[arr[i], arr[i + 1]] = [arr[i + 1], arr[i]]
          // Send an update after the swap
          postMessage({ array: arr })
          swapped = true
        }
      }
    } while (swapped)

    return arr
  }
  const sortedArray = bubbleSort(unsortedArray)

  // Send the fully sorted array after all updates
  postMessage({ array: sortedArray })
}
