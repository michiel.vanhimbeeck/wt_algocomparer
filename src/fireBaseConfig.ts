// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getDatabase } from 'firebase/database'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyAsAn7KAqZWNUUR0GZdgNes-GfPDkAwU9s',
  authDomain: 'wtdatasets.firebaseapp.com',
  databaseURL: 'https://wtdatasets-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'wtdatasets',
  storageBucket: 'wtdatasets.appspot.com',
  messagingSenderId: '532946765688',
  appId: '1:532946765688:web:76cb8ef95153a07b282a8d'
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)

// Initialize Realtime Database and get a reference to the service
const database = getDatabase(app)

export default database
