

export const useLabelGenerator = (aantal: number): Array<number> => {
    const arr: Array<number> = [];
    for (let i = 0; i < aantal; i++) {
        arr.push(i);
    }
    return arr;
}